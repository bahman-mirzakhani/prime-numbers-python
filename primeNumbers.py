number = int(input("Enter your number: "))

primes = []
noPrimes = []

for m in range(1, number + 1):
    flag = 0
    for n in range(2, m):
        if (m % n == 0) and (m != 1):
            print("The {:>3} is not prime! Because {}*{}".format(m, m//n, n))
            noPrimes.append(m)
            flag = 1
            break
    if flag != 1:
        print("The {:>3} is prime".format(m))
        primes.append(m)
        
print("\nThe prime numbers are:\n", primes)
print("\nThe not prime numbers are:\n", noPrimes)
